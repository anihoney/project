import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 30}}>Open up App.js to start working on your app!</Text>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: 
   {
       backgroundColor: '#fff',
       alignItems: 'center',
       justifyContent: 'center',
      padding: 30,
      marginTop: 65,
    },
    flowRight: {
      flexDirection: 'row',
      alignItems: 'center',
      alignSelf: 'stretch',
    },
    searchInput: {
      height: 36,
      padding: 4,
      marginRight: 5,
      flexGrow: 1,
      fontSize: 18,
      borderWidth: 1,
      borderColor: '#48BBEC',
      borderRadius: 8,
      color: '#48BBEC',
     },
   });